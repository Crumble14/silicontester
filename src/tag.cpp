#include "silicon.hpp"

#define BUFFER_SIZE	128

using namespace Silicon;

static string get_info(const string& line, const size_t index)
{
	size_t pos = 0;

	for(size_t i = 0; i < index; ++i) {
		if((pos = line.find('\t', pos)) == string::npos) break;
		++pos;
	}

	string str(line.cbegin() + pos, line.cend());

	if((pos = str.find('\t')) != string::npos) {
		str = string(str.cbegin(), str.cbegin() + pos);
	}

	return str;
}

vector<Tag> Silicon::get_tags()
{
	FILE* process = popen("ctags -R -f - --languages=c,c++", "r");
	stringstream ss;

	string buff;
	buff.reserve(BUFFER_SIZE);

	while(!feof(process)) {
		if(fgets(buff.data(), BUFFER_SIZE, process)) ss << buff.data();
	}

	pclose(process);

	vector<Tag> tags;

	while(getline(ss, buff)) {
		if(buff.empty()) continue;
		if(get_info(buff, 3) != "f") continue;

		tags.emplace_back(get_info(buff, 0), get_info(buff, 1));
	}

	return tags;
}
