#include "silicon.hpp"

#define DEFAULT_COMPILE_FLAGS	"-Wall -Wextra -Werror -std=c++17 -O3 -lstdc++ -lm"

using namespace std;

int main()
{
	using namespace Silicon;

	cout << "Starting SiliconTester..." << '\n';
	cout << "Getting tags..." << '\n';

	const auto tags = get_tags();

	if(tags.empty()) {
		cerr << "No tag found, exiting..." << '\n';
		return 0;
	}

	cout << "Looking for unit tests..." << '\n';

	const auto tests = get_unit_tests(tags);

	cout << "Compiling test binary..." << '\n';

	// TODO Take compile flags in option
	if(!compile_tests(tests, COMPILE_FLAGS)) {
		cerr << "Tests compilation failed!" << '\n';
		return -1;
	}

	cout << "Beginning tests..." << '\n';

	// TODO

	return 0;
}
